![caliburn](img/caliburn.jpg)

# **3D printed Nerf-Gun**

> Michael Hedl
>
> based on captain slug's work: http://www.captainslug.com
> most 3D models downloaded from: https://www.thingiverse.com/thing:2376150

## Caliburn!

This is a 3D printable gun that shoots foam projectiles from original [Nerf Guns](https://nerf.hasbro.com/worldwide). Those are kids toys. The Caliburn takes these darts to the next level and maybe up to their limits. It is compatible with the original Nerf N-Strike Elite magazines. Almost all of the non-printable parts can be obtained from local hardware stores. So go 4 it and build your own.

## Metric Parts!

The original design by captain slug uses Imperial Units for the hardware and printed parts. I am located in Austria and we only use metric units here, so the design was altered a bit to fit the metric equivalents. For all parts in the original, there are references to the metric counterparts. Most non printable parts are sourced from local hardware stores (Obi, Bauhaus). For all other parts I will provide links to Amazon.de or german online Shops. The estimated total cost is about 35€ for a single unit, excluding shipping costs and printed parts. 

## Upgrades!

The original design comes with several upgrades. Some are purely cosmetic, others with a purpose. Here is a list with all upgrades for this specific model.

- Original design: [Caliburn Mag-Fed Pump-Action Nerf Blaster](https://www.thingiverse.com/thing:2376150)
- Stock Spacer: [Caliburn - Printed Stock Spacers](https://www.thingiverse.com/thing:3105159)
- Stock: [Caliburn - Adjustable Stock](https://www.thingiverse.com/thing:3209370)
- Stock Cosmetic: [Nerf Caliburn - Cosmetic Stock Spacers](https://www.thingiverse.com/thing:2849092)
- Barrel Shroud: [Nerf Caliburn - Railgasm Barrel Shroud](https://www.thingiverse.com/thing:2722395)
- Muzzle Device: [Nerf Caliburn - "Suppressor" SCAR Barrel](https://www.thingiverse.com/thing:2863865)
- Grip Insert:[Nerf Caliburn - Grip Insert](https://www.thingiverse.com/thing:2849364)
- Foregrip: [Caliburn - Pyrangle Foregrip](https://www.thingiverse.com/thing:3180697)
- Tactical Laser: my own design.

All of the Upgrades can be found online (Tactical Laser excluded). To ensure compatibility, I have included them in this repository. Downloaded February 2021.

The design (in terms of colors) was done in the [official color configurator by captain slug](http://www.captainslug.com/nerf/caliburn/e/#eyJuYW1lIjoiQ2FsaWJ1cm4gUmVkZGl0Iiwic2xvdHMiOnsiQmFzZSI6eyJuYW1lIjoiQ29yZSBDYWxpYnVybiIsIm1vZGVscyI6eyJTcGFjZXIgVHViaW5nIjp7Im5hbWUiOiJTcGFjZXIgVHViaW5nIiwiZmlsZSI6Il9ueWxvbnNwYWNlcnMuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJSYWlsIjp7Im5hbWUiOiJSYWlsIiwiZmlsZSI6InJhaWwuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJNaWxhbiBDb3VwbGVyIjp7Im5hbWUiOiJNaWxhbiBDb3VwbGVyIiwiZmlsZSI6Ik1pbGFuQ291cGxlci5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlBsdW5nZXIiOnsibmFtZSI6IlBsdW5nZXIiLCJmaWxlIjoicGx1bmdlci5zdGwiLCJjb2xvciI6Ik9yYW5nZSJ9LCJUcmlnZ2VyIjp7Im5hbWUiOiJUcmlnZ2VyIiwiZmlsZSI6InRyaWdnZXIuc3RsIiwiY29sb3IiOiJPcmFuZ2UifSwiU2VhciI6eyJuYW1lIjoiU2VhciIsImZpbGUiOiJzZWFyLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiTXV6emxlIjp7Im5hbWUiOiJNdXp6bGUiLCJmaWxlIjoibXV6emxlLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiVHJlbmNoUCI6eyJuYW1lIjoiVHJlbmNoUCIsImZpbGUiOiJ0cmVuY2hwLnN0bCIsImNvbG9yIjoiT3JhbmdlIn19fSwiTWFnd2VsbCI6eyJuYW1lIjoiRUxJVEUiLCJtb2RlbHMiOnsiRWxpdGUgVXBwZXIiOnsibmFtZSI6IkVsaXRlIFVwcGVyIiwiZmlsZSI6IkVsaXRlVXBwZXIuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJFbGl0ZSBMb3dlciI6eyJuYW1lIjoiRWxpdGUgTG93ZXIiLCJmaWxlIjoiRWxpdGVMb3dlci5zdGwiLCJjb2xvciI6IldoaXRlIn0sIk1hZ1JlbGVhc2UiOnsibmFtZSI6Ik1hZ1JlbGVhc2UiLCJmaWxlIjoibWFncmVsZWFzZTIuc3RsIiwiY29sb3IiOiJPcmFuZ2UifX19LCJHcmlwIjp7Im5hbWUiOiJNb25vbGl0aGljIEdyaXAiLCJtb2RlbHMiOnsiVHJpZ2dlciBHdWFyZCI6eyJuYW1lIjoiVHJpZ2dlciBHdWFyZCIsImZpbGUiOiJ0Z3VhcmQuc3RsIiwiY29sb3IiOiJCbGFjayJ9LCJHcmlwIjp7Im5hbWUiOiJHcmlwIiwiZmlsZSI6ImdyaXAuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJTdG9ja19LaXJpIjp7Im5hbWUiOiJTdG9ja19LaXJpIiwiZmlsZSI6InN0b2NrX2tpcmkuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9fX0sIkhhbmRndWFyZCI6eyJuYW1lIjoiTm9uZSIsIm1vZGVscyI6e319LCJHcmlwIEluc2VydCI6eyJuYW1lIjoiWUVTIiwibW9kZWxzIjp7IkdyaXAgSW5zZXJ0Ijp7Im5hbWUiOiJHcmlwIEluc2VydCIsImZpbGUiOiJncmlwaW5zZXJ0LnN0bCIsImNvbG9yIjoiQmxhY2sifX19LCJTdG9jayBDb3NtZXRpYyI6eyJuYW1lIjoiVGh1bWJob2xlIiwibW9kZWxzIjp7IlRodW1iaG9sZSI6eyJuYW1lIjoiVGh1bWJob2xlIiwiZmlsZSI6InN0b2NrMy5zdGwiLCJjb2xvciI6IldoaXRlIn19fSwiQmFycmVsIFNocm91ZCI6eyJuYW1lIjoiUmFpbGdhc20iLCJtb2RlbHMiOnsiUmFpbGdhc20gRmdyaXAiOnsibmFtZSI6IlJhaWxnYXNtIEZncmlwIiwiZmlsZSI6ImIxX2ZvcmVncmlwLnN0bCIsImNvbG9yIjoiT3JhbmdlIn0sIlJzZWdtZW50MSI6eyJuYW1lIjoiUnNlZ21lbnQxIiwiZmlsZSI6ImIxYS5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlJzZWdtZW50MiI6eyJuYW1lIjoiUnNlZ21lbnQyIiwiZmlsZSI6ImIxYi5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlJzZWdtZW50MyI6eyJuYW1lIjoiUnNlZ21lbnQzIiwiZmlsZSI6ImIxYy5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlJzZWdtZW50NCI6eyJuYW1lIjoiUnNlZ21lbnQ0IiwiZmlsZSI6ImIxZC5zdGwiLCJjb2xvciI6IldoaXRlIn19fSwiRm9yZWdyaXAiOnsibmFtZSI6IlB5cmFuZ2xlIEZvcmVncmlwIiwibW9kZWxzIjp7IlBncmlwIjp7Im5hbWUiOiJQZ3JpcCIsImZpbGUiOiJwZ3JpcDEuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJQZ3JpcCBDb3JlIjp7Im5hbWUiOiJQZ3JpcCBDb3JlIiwiZmlsZSI6InBncmlwMi5zdGwiLCJjb2xvciI6IkJsYWNrIn19fSwiTXV6emxlIERldmljZSI6eyJuYW1lIjoiU0NBUiBTdXBwcmVzc29yIiwibW9kZWxzIjp7IlNDQVIgZnJvbnQiOnsibmFtZSI6IlNDQVIgZnJvbnQiLCJmaWxlIjoic2Nhcl9mcm9udC5zdGwiLCJjb2xvciI6Ik9yYW5nZSJ9LCJTQ0FSIGJhY2siOnsibmFtZSI6IlNDQVIgYmFjayIsImZpbGUiOiJzY2FyX2JhY2suc3RsIiwiY29sb3IiOiJXaGl0ZSJ9fX0sIlN0b2NrIFR5cGUiOnsibmFtZSI6IkFkanVzdGFibGUiLCJtb2RlbHMiOnsiQVN0b2NrMWEiOnsibmFtZSI6IkFTdG9jazFhIiwiZmlsZSI6IkFTdG9jazFhLnN0bCIsImNvbG9yIjoiQmxhY2sifSwiQVN0b2NrMmEiOnsibmFtZSI6IkFTdG9jazJhIiwiZmlsZSI6IkFTdG9jazJhLnN0bCIsImNvbG9yIjoiT3JhbmdlIn0sIkFTdG9ja1BhZCI6eyJuYW1lIjoiQVN0b2NrUGFkIiwiZmlsZSI6IkFTdG9ja1BhZC5zdGwiLCJjb2xvciI6IkJsYWNrIn19fSwiSXJvbiBTaWdodHMiOnsibmFtZSI6Ik5vbmUiLCJtb2RlbHMiOnt9fSwiUmFpbCBSaXNlciI6eyJuYW1lIjoiTm9uZSIsIm1vZGVscyI6e319fX0=). 
Color scheme was first seen at [this reddit thread](https://www.reddit.com/r/3Dprinting/comments/bmdn1g/3d_printed_nerf_rifle_anyone/). 
I suspect it is strongly influenced by the [Asiimov AWP skin from Counter Strike: Global Offensive](https://steamcommunity.com/sharedfiles/filedetails/?id=194005009).

## Hardware!

The original hardware list is somewhat confusing. Details in the [Log](log.md). To start a whole new Caliburn these are the minimum metric parts that are required:

- Main Spring: [K25 spring from OutOfDarts](https://outofdarts.com/products/k25-spring-with-squared-ends-better-than-mcmaster?_pos=1&_sid=0589a9723&_ss=r)
  
  *This was the hardest part to get. I wanted this to be as close to the original as possible, hence the "easiest" option was to order the same spring as the original.*
  
- Plunger Tube: [Acryl tube from Modulor](https://www.modulor.de/acrylglas-xt-rundrohr-satiniert-o-38-0-x-35-0-l-330.html)
  
  *This was the second hardest part to find. It has to match the imperial parts very closely, as this part provides the pressure that propels the dart. Any tube that has 34mm inner diameter and around 38mm outer diameter should be sufficient. Length: 185mm*
  
- Other Springs: [This spring set from Amazon](https://www.amazon.de/gp/product/B088K2ZKLH/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
  
  *Used for the trigger mechanism and the mag release lever.*
  
- Threaded Rods: [M5 rods from Obi](https://www.obi.at/gewindestangen/lux-gewindestangen-m5-x-1-000-mm-verzinkt/p/7608839)
  
  *6x330mm and 1x210mm*
  
- Spacers: [ABS tube from Obi](https://www.obi.at/profile/rundrohr-weiss-7-mm-x-7-mm-x-1000-mm/p/3673274)
  
  *Tubes that slide over the threaded rods. 2x290mm and 1x160mm. Outer Diameter: 7mm, Inner Diameter 5mm* 
  
- Barrel: [Aluminium tube/pipe from Obi](https://www.obi.at/profile/rundrohr-silber-eloxiert-15-mm-x-15-mm-x-1000-mm/p/1573526)
  
  *With an inner diameter of 13mm it fits the Nerf darts perfectly. Outer diameter: 15mm, Length: 500mm*
  
- Bolt Arm: [Aluminium bar from Obi](https://www.obi.at/profile/flachstange-silber-eloxiert-2-mm-x-15-mm-x-1000-mm/p/1573468)
  
  *12x2mm cross section max. If hard to find, use 10x2mm. Length: 370mm*
  
- Rubber Gaskets: [O-ring set from Amazon](https://www.amazon.de/gp/product/B01FNVV0WC/ref=ppx_yo_dt_b_asin_title_o00_s01?ie=UTF8&psc=1)
  
  *I used the R-22 from this kit. Inner diameter 28mm, thickness: 3.5mm. I also modded the Plunger Tube and Ramrod for these O-rings to fit them better.*
  
- Clevis Pins: [5mm flat headed pins from Amazon](https://www.amazon.de/gp/product/B081V586Z2/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
  
  *Needed 3x. The original design fits M4x50 screws perfectly. I used a 5mm drill to widen the holes to use easier to find 5mm pins instead.*
  
- M5x8 Screws
  
  *A bunch of these. Easy to find, use whatever fits your toolkit best. Directly replaces 10-32 Imperial Screws.*
  
- M5x50 Screws
  
  *Effectively used 1 Screw. Could be left out.*
  
- M5 Hex Nuts
  
  *A bunch of these. Easy to find. They are smaller than the imperial ones, so they rotate in the cavities. Used pliers to tighten them, instead of modding existing parts.*
  
- M3x10 Screws
  
  *A bunch of these. Easy to find, use whatever fits your toolkit best. Directly replaces 4-40 Imperial Screws. No M3 nuts needed.*
  
- M3x10 Standoff
  
  *A single one. Used for the pivoting mechanism of the trigger.*
  
- 2x20mm Steel Pins
  
  *Used to secure the triggers. I simply cut 2mm nails to size.* 

Other parts are somewhat unnecessary are:

- Felt or sponge rubber
  
  *To dampen the sound of the plunger tube and ramrod. Use whatever you find and cut it to size.*
  
- Ramrod
  
  *This is a printed part. Original assembly instructions state to get an aluminium rod for this. Just print the part as a whole, works fine.*
  
- Stock Spacer
  
  *Could not find nice tube, so I printed this also. This is a non critical part, as it acts only as a housing for the spring. Benefit of printing this: you can choose the color and design.*
  
- Fishing Line
  
  *Used for the "suppressor". It actually introduces a spin to the dart, to improve its trajectory. Added accuracy is more important than raw power to me.*

## Performace!

This gun kicks. 

It is somewhat accurate with cloned darts. Legends say accuracy can be improved with short, stiff darts. I used the cheapest clones I could find. At a distance of 10m, a 30cm target can be hit 8/10 times.

Shot at a cardboard box from 10m, the darts go straight through, even the ones with a soft head. Sadly I have no way to measure the speed of the darts. But the Internet says they can easily go up to 200km/h. 

**That hurts. Do not shoot at people. Please.**

![out_slomo](img/out_slomo.gif)

This GIF shows darts shot from 3m. It is a slow motion shot. This is triple layered cardboard, so the darts do not go through, but at least get stuck. 

## Images!

![D85_2369](img/D85_2369-6536471.jpg)

![D85_2372](img/D85_2372.jpg)

![D85_2377](img/D85_2377.jpg)

