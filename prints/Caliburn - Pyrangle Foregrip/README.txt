                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3180697
Caliburn - Pyrangle Foregrip by captainslug is licensed under the Creative Commons - Public Domain Dedication license.
http://creativecommons.org/publicdomain/zero/1.0/

# Summary

CHANGE NOTE 3/10/19 - Completely redesigned for added strength, comfort, and ease of printing.

CHANGE NOTE 1/31/19 - Changed print orientation of Pgrip_2ta

CHANGE NOTE 1/11/19 - Let's go extra THICC

CHANGE NOTE 11/27/18 - MORE MEAT on Pgrip_2t and an optional thru-hole for a bolt.

CHANGE NOTE 10/29/18 - Small changes to all files, naming structure changed.

Mounts to picatinny rail. Comes in 2-part and 3-part varieties to allow for variety in color blends. 3-part design is also more likely to be compatible with being printed partly in TPU.

2-part design needs a minor tweak as the screw hole on the bottom doesn't quite line up yet when assembled onto a rail segment.

# Print Settings

Supports: No
Infill: 300 Micron