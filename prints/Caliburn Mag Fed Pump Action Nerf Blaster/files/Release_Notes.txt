CALIBURN Mag-Fed Pump-Action Homemade Nerf Blaster (10/04/17 Update)

CHANGE NOTE 06/26/17: I have added a chamfer to the print-bed side of some of the parts that previously lacked one. I did this after having had some difficult with removing those parts from my build plate after printing them. I have also scaled all of the parts to millimeters.

CHANGE NOTE 06/29/17: Revised BUTT STL file to fixed potential issue with plunger being primed too far back.

CHANGE NOTE 06/30/17: Removed thin support piece in Stock_Alt2. Shrank OD of Plunger assembly parts.

CHANGE NOTE 07/11/17: Updated P1 and P2 to move skirt seal 1/8" further forward to remove potential for it to be primed beyond the aluminum plunger tube. Reamed out a few parts to improve assembly consistency, strengthened P3b. Parts should now print with all holes to tolerance so that post-drilling will not be require prior to assembly. Also included some optional parts such as alternative grip plates, ring that center the 1.5" OD plunger tubes in the 1.66" ID features of the part, a foregrip with fewer rail segments, and a non-cushioned buttplate. Muzzle can now be tapped for 10-32 screws to retain the barrel if you want to avoid gluing the barrel to it.

CHANGE NOTE 07/13/17: Fixed length error in P1B part

CHANGE NOTE 07/29/17: Tolerance changes to BOLT1 piece to ease assembly. Minor changes to GripSpacer halves.

CHANGE NOTE 08/11/17: Strengthened upper tab of SPREADER and slightly widened the height of the slots. Added web to STOCK_ALT2. Added chamfers to RAM2 to reduce potential for fractures.

CHANGE NOTE 08/30/17: Moved a few parts out of the 100percentinfill folder. After printing a ton of these not as many parts require being printed solid as I had previously assumed. I have also added some RAM2 variations to the Optional parts folder. One doesn't include the o-ring undercut so it will be more durable than the standard, but will lose 10% of its muzzle velocity compared to using an o-ring with the standard RAM2. The other file is the same, but has a smaller ID and could potentially be used to handicap performance below 150fps so that your blaster will comply with the majority of HvZ rules.

CHANGE NOTE 10/04/17: Changed MAGWELL Inside profile to improve compatibility with JET Katana Mag adapter. Widened through holes in a variety of parts to reduce the likelihood for drilling after print. Narrowed JAM part to make it compatible with Artifact P-Mags. Plunger and Ram components are now available for both O-Rings and Skirt seals. O-ring design has no leaks, but skirt seal option is still available for backwards compatibility purposes.

CHANGE NOTE 10/05/17: Hardware compatibility widened for RAM2o recesses.

CHANGE NOTE 10/07/17: O-Ring land tolerances

CHANGE NOTE 10/08/17: Single-piece O-Ring Plunger

CHANGE NOTE 10/12/17: Changes to Plunger and Ram2O to address vacuum-loading issues

CHANGE NOTE 10/21/17: Cosmetic changes to multiple parts. Changed the "Ansuzalgiz" stock spacer part to include a third leg so extra strength

CHANGE NOTE 10/24/17: Increased the size of the spring pegs on SEAR and TRIGGERALT

CHANGE NOTE 11/1/17: Combined the Grip panels with their spacer parts. Lengthened Stock_Alt to make up the difference. SEAR now uses a 1/4" OD standoff as an axle. Magwell now include a hook for the extension spring rather than using a second spring pin.

CHANGE NOTE 11/15/17: BUTT is now split into two parts to allow for swapping the mainspring without disassembly. RAM2O and PLUNGER no longer need to be printed at 100% (just make sure to use perimeters of 1mm or thicker).

CHANGE NOTE 12/11/17: Small changes to tolerances on Ram2o so that even prints that have flash on the underside edge of the print will still fit nicely in the plunger tube.

CHANGE NOTE 12/26/17: Monolithic Grip, DartJam, and Ryan Tube added.

CHANGE NOTE 1/27/18: Updated RAIL_TOP and STOCK_ALT5 parts to include holes for spring pins that will prevent the rails from rocking side-to-side.

CHANGE NOTE 3/17/18: Many small changes. Most of them deal with print quality and reducing time needed to finish or deburr parts during assembly. Spring peg of SEAR5 updated to match the one of the MagRelease so that the spring is more captive. Magwell and Grip both now include through-holes for their short pins through both sides of the part so that disassembly or extraction of the short pin is possible. Muzzle piece now uses 4-40 lock nuts and 4-40 screws to act as set screws for retaining the barrel.

CHANGE NOTE 5/07/18: Rolled the "THICC" Stock parts, Shadwell, and Captive Nut Spreader into the standard file set.

CHANGE NOTE 6/06/18: 4-40 thin hex nuts have been completely removed from the hardware set. 4-40 screws now simply drive into the prints. Replaced "Pizza Table" and "Stock_Alt" with "Stock_Kiri". Monolithic magwell is going to be the standard magwell from here forward. SEAR5 and Ram2oe should be printed with 2mm perimeters.


Released as PUBLIC DOMAIN file set by Captain Slug (captainslg@aol.com)

All files are scaled in millimeters and oriented for printing.

Items provided in the sub-folder "100percentinfillparts" obviously need to be printed at 100% infill. 
All parts are printable and functional using PLA filament. ABS will obvious be stronger, but isn't required.