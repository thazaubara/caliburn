# Caliburn Build Log

3D gedruckte Nerf-Gun

# Linksammlung

Reddit Base Thread:
https://www.reddit.com/r/3Dprinting/comments/bmdn1g/3d_printed_nerf_rifle_anyone/

Thingiverse:
https://www.thingiverse.com/thing:2376150

Metric Caliburn Remix: (glaub braucht man net wenn man nächsten link beachtet)
https://www.thingiverse.com/thing:3017498

Deutsche Maße vom Baumarkt:
https://blasted.de/threads/caliburn.6668/ (Einkaufsliste Seite 2)

# Mechanik

![Screenshot 2021-01-04 at 21.04.22](img/Screenshot 2021-01-04 at 21.04.22.png)

## Kosten

Ein Hardware Set (Versand geteilt durch 2): **65,61€** (exkl. lagernde Teile)

Privater Google-Sheets [Link](https://docs.google.com/spreadsheets/d/15kYnqknnirfZKMuPziVLJkbdDCfoA4Ae/edit#gid=1696340201) zur Berechnung

## Federn

### :white_check_mark: K25 Druckfeder

Original gekauft bei https://outofdarts.com

> :mag: K25 Spring (1)
> **K26 Compression Spring, Spring-Tempered Steel, 11.0" Long,.844" OD, .08" Wire**
>
> Diameter: 0,968" -> **2.46cm**
> Len: 11" -> **27,94cm**
> Wire: 0,08" -> **0,2cm**
>
> Info: http://nerfhaven.com/forums/topic/21855-the-nic-spring-database/

> Name: [k26]
> OD: .844" ID: .688"
> Length (As sold by McMaster): 11"
> McM Constant: 131.9
> Coils/Inch: 3.09
> Notes: A favorite among modders as a replacement spring. Very powerful, but also very good at breaking stuff if not properly reinforced.
>
> Name: [k25]
> OD: .968" ID: .808"
> Length (As sold by McMaster): 11"
> McM Constant: 84
> Coils/Inch: 2.18
> Notes: Good replacement spring to get power out of blasters with little to no reinforcement.
>
> Name: K14
> OD: 1.09" ID: 0.88"
> Length: (As sold by McMaster): 11"
> Coils/Inch: 2.45
> Coils/Spring: 27
> McM Constant: 181.2
> Notes: Stupidly strong. Almost always used in homemades because factory plastic can't take it.

### :white_check_mark: Zugfedern

Zugfedern Pack bei [Amazon](https://www.amazon.de/gp/product/B088K2ZKLH/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)

5,5 x 25,5 x 0,6mm

> :mag: Extension Springs (16)

> Dazu 3 Zugfedern mit Größe: 5,5 x 25,5 x 0,6mm da ginge aber auch paar mm länger.

## Gewindestangen

### :white_check_mark: M5 Gewindestangen

OBI:
[M5x1m](https://www.obi.at/gewindestangen/lux-gewindestangen-m5-x-1-000-mm-verzinkt/p/7608839)

Zuschnitt (ges. ~220cm):
1x 20,3 cm
6x 33,5 cm



Mein Zuschnitt:

Hinten: 3x 330mm

Griff: 215mm

Vorne: 3x 360mm

> **Low-Strength Steel Threaded Rod, Zinc Plated, 10-32 Thread, 6 Feet Long, Fully Threaded** 
> **six at 13.25" length, one at 8" length**
>
> :mag: 13" Threaded Rod (8)
>
> *10-32, 13 inch*
>
> :mag: 8" Threaded Rod (13)
>
> *10-32, 8 inch*

<img src="img/Screenshot 2021-01-09 at 11.14.30.png?lastModify=1610188704" alt="Screenshot 2021-01-09 at 11.14.30" style="zoom:25%;" />

### :white_check_mark: Spacer

Obi:
[ABS Schwarz 7/5](https://www.obi.at/profile/rundrohr-schwarz-7-mm-x-7-mm-x-1000-mm/p/4800009)
[ABS Weiß 7/5](https://www.obi.at/profile/rundrohr-weiss-7-mm-x-7-mm-x-1000-mm/p/3673274)
[Alu 8/6](https://www.obi.at/profile/rundrohr-silber-eloxiert-8-mm-x-8-mm-x-1000-mm/p/1573492)

Rohr:
AD=7mm
ID=5mm

Zuschnitt: 
3x 28,6cm



> **Nylon Tubing, 5/16" OD, 0.04" Wall Thickness three at 11.25" length, one at 6" length**
>
> :mag:11.25" Spacer (6)
>
> :mag: Spacer 6,625 (35)

> Rohre (Alu oder Plastik) mit **7AD, 5ID 3x28,6cm**
>
> Original:
> AD=8mm
> ID=5mm

<img src="img/Screenshot 2021-01-09 at 11.14.55.png" alt="Screenshot 2021-01-09 at 11.14.55" style="zoom: 15%;" />

## Acryl Rohre

###  :white_check_mark: Stock Spacer

nur housing für feder, kann auch drucken
https://www.thingiverse.com/thing:3105159

AD=42 mm
ID=36 mm
d=3 mm
l=76 mm

https://www.kus-kunststofftechnik.de/polycarbonat-rohr/polycarbonat-rohr-o-38-mm-laenge-waehlbar-von-100-bis-1400-mm/

https://www.modulor.de/acrylglas-xt-rundrohr-farblos-o-40-0-x-36-0-l-330.html



**Maße bedingt durch Modell/Druck:**

Druckteil Maße:
ID=min. 34,29mm
AD=min. 42,8mm (42.2 im druck)

AD=1-1/4 (1,66)
ID= (1,42)

> :mag: Stock Spacer Alt A2 (2)
> **Stock Spacer (1-1/4 SCd ID) one at 3" length**



<img src="img/Screenshot 2021-01-08 at 20.29.51.png" alt="Screenshot 2021-01-08 at 20.29.51" style="zoom: 15%;" />

### :white_check_mark: Plunger Tube

auf Modulor bestellt

zach weil inch die penner.

AD=38 mm
ID=34 mm
d=2 mm
l=185 mm

https://www.kus-kunststofftechnik.de/polycarbonat-rohr/polycarbonat-rohr-o-38-mm-laenge-waehlbar-von-100-bis-1400-mm/

https://www.modulor.de/acrylglas-xt-rundrohr-satiniert-o-38-0-x-35-0-l-330.html (16€ versand)

**Maße bedingt durch Modell/Druck:**

**Modell:**

O-Ring (ID): 34,925mm
Housing für Tube (AD): 42,9mm (Zeichnung zu groß)
Plunger (ID): 33,5mm
Milancoupler (AD), 38,8mm

**Druck:** 

Housing für Tube (AD): 38mm
Plunger (ID): 34mm
Ramrod (ID): 34,3mm
Milancoupler: 38,3mm

**Resumè: 38/35 sollte perfekt passen.**

AD=1-1/2 (1,5)
ID=1-3/8 (1,375)

> :mag: Plunger Tube (4)
> **Polycarbonate Round Tube, 1-1/2" OD, 1-3/8" ID, Clear, 8' Long**, one at 7.313" length

> *Für den Plunger und Federschutz: **DN40 Abflussrohr 18,7cm und 7,6cm***

<img src="img/Screenshot 2021-01-08 at 20.04.19.png" alt="Screenshot 2021-01-08 at 20.04.19" style="zoom:15%;" />

> 

## Alu Rohre

### :white_check_mark: Lauf

OBI:
[Alu Rohr 15/13](https://www.obi.at/profile/rundrohr-silber-eloxiert-15-mm-x-15-mm-x-1000-mm/p/1573526)

AD = 15 mm
ID = 13 mm
L =   länger

> :mag: Barrel (15)
> **Highly Corrosion-Resistant 6063 Aluminum, Architectural, .625" OD, 0.049" Wall Thickness, .527" ID one at 14" length**

> Für den Lauf habe ich ein **Alurohr genommen mit 15AD und 13ID,** allerdings sitzen die Darts da schon straff drin, mir war nur das Messingrohr zu teuer...

<img src="img/Screenshot 2021-01-09 at 11.15.20.png" alt="Screenshot 2021-01-09 at 11.15.20" style="zoom:15%;" />

### :white_check_mark:  Ramrod

Gedruckt

> :mag:RamRod Core (38)
> **Aluminum Ramrod Core, 1/2" OD x .344" ID** one at 5.75" length

> Dazu braucht man noch irgend einen Kunststoff-Rundstab mit **16mm Dicke und 9cm lang**. Ich hab mir den einfach Gedruckt. 

<img src="img/Screenshot 2021-01-09 at 11.17.51.png" alt="Screenshot 2021-01-09 at 11.17.51" style="zoom:15%;" />

## Alu-Flachstange

### :white_check_mark: Bolt-Arm

auf modulor bestellt

Stahl: https://www.bauhaus.at/metallprofile-kunststoffprofile/kantoflex-flachstange/p/10527253
Messing: https://www.bauhaus.at/metallprofile-kunststoffprofile/kantoflex-flachstange/p/10506085
15x2 Alu (Umbau) : https://www.obi.at/profile/flachstange-silber-eloxiert-2-mm-x-15-mm-x-1000-mm/p/1573468

Bohrungen: http://www.captainslug.com/nerf/boltarm.gif

> :mag:BoltArm (24)
> **Multipurpose 6063 Aluminum, Rectangular Bar, 3/32" x ½", 2 foot, two at 14.5" length**

> Alu-4kan Stab 10x2 (glaub ich hab 10x3 nur gefunden) **2x 36,7cm**
> eigentlich **12.7*2.36mm x 36.8 cm** (2x)

<img src="img/Screenshot 2021-01-08 at 20.25.37.png" alt="Screenshot 2021-01-08 at 20.25.37" style="zoom:15%;" />

## Dichtungen

### :white_check_mark: Silikon Pad

auf modulor bestellt. wird selbst gebaut

Selbstbau ?

3mm dick

> :mag: Shock Pad (11)
> **Shock Pad (1/8" thick adhesive-backed silicone, 45A Durometer)1.25" OD x .5625" ID**

<img src="img/Screenshot 2021-01-09 at 11.17.09-0187605.png" alt="Screenshot 2021-01-09 at 11.17.09" style="zoom:15%;" />

<img src="img/Screenshot 2021-01-09 at 11.17.22.png" alt="Screenshot 2021-01-09 at 11.17.22" style="zoom:15%;" />

### :white_check_mark: O-Ring

Amazon [Set](https://www.amazon.de/gp/product/B01FNVV0WC/ref=ppx_yo_dt_b_asin_title_o00_s01?ie=UTF8&psc=1) gekauft. 

Ø 31 mm, Stärke 3,5 mm



R-05 klein (10/1,5)

R-22 Groß (28/3,5)

> :mag:Dash 123 O-Ring (29)
> **O-Ring Dash 123 (1-3/8" Od x 1-3/16" ID)**

> Dazu braucht man noch 3 O-Ringe 2stk 31,2 x3 und 1 kleiner für den Lademechanismus (Ram) da müsste ich aber nochmal nachmessen.

## Schrauben & Muttern

### :white_check_mark: M5x8 Schrauben

lagernd

> :mag:Screws (14) 
> **Steel Rounded Head Screws with External-Tooth Lock Washer, 10-32 Thread Size, 5/16" Long**

### :white_check_mark: M5x45 Schrauben

[Obi](https://www.obi.at/gewindeschrauben/lux-zylinderkopf-gewindeschraube-m5-x-50-mm-verzinkt-schlitz-8-stueck-mit-muttern/p/7608177)

> :mag:1-3/4 Length Screw (37)

### :white_check_mark: M5 Muttern

M5 Selbstsichernde Muttern

> :mag:10-32 Hex Nuts (39)

### :white_check_mark: M3 Schrauben

M3x10 
(eventuell M3x8)

[M3 Senkkopf](https://www.obi.at/gewindeschrauben/lux-senkkopf-gewindeschraube-m3-x-10-mm-verzinkt-schlitz-40-stueck-mit-muttern/p/7607138)

> :mag:4-40 Short Screw (30)
> **Flat Head Screw, 4-40 Thread Size, 3/8" length**

### :white_check_mark: M3 Standoff

M3x8 Standoff

> :mag:4-40 Standoff (34)
> **Female Threaded Round Standoff, Aluminum, 1/4" OD, 11/32" Long, 4-40 Thread Size**

### :white_check_mark: 2mm Stahlstifte

[Stift](https://www.obi.at/naegel-sets/lux-spannstift-2-mm-x-30-mm-gehaertet-8-stueck/p/8465247)
[Nagel](https://www.obi.at/naegel-sets/lux-linsenkopf-stahlnagel-2-mm-x-20-mm-geblaeut-50-stueck/p/8466591)

2x20mm

> :mag:Pin Short (26)
> **1/6 x 3/4" steel spring pins**

### :white_check_mark: M5 Spring Pins

sourcing map 10 Pcs Flat Head Single Hole Fork Head Pins 5mm x 50mm 304 Stainless Steel

https://www.amazon.de/gp/product/B081V586Z2/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1

> :mag:Takedown Pin (36)
> **Zinc-Plated Steel Clevis Pin, 3/16" Diameter, 1-13/16" Usable Length**

<img src="img/Screenshot 2021-01-09 at 12.28.35.png" alt="Screenshot 2021-01-09 at 12.28.35" style="zoom:15%;" />





# Druckteile

Config-Link zur Extended Version (Bild): [Link](http://www.captainslug.com/nerf/caliburn/e/#eyJuYW1lIjoiQ2FsaWJ1cm4gUmVkZGl0Iiwic2xvdHMiOnsiQmFzZSI6eyJuYW1lIjoiQ29yZSBDYWxpYnVybiIsIm1vZGVscyI6eyJTcGFjZXIgVHViaW5nIjp7Im5hbWUiOiJTcGFjZXIgVHViaW5nIiwiZmlsZSI6Il9ueWxvbnNwYWNlcnMuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJSYWlsIjp7Im5hbWUiOiJSYWlsIiwiZmlsZSI6InJhaWwuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJNaWxhbiBDb3VwbGVyIjp7Im5hbWUiOiJNaWxhbiBDb3VwbGVyIiwiZmlsZSI6Ik1pbGFuQ291cGxlci5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlBsdW5nZXIiOnsibmFtZSI6IlBsdW5nZXIiLCJmaWxlIjoicGx1bmdlci5zdGwiLCJjb2xvciI6Ik9yYW5nZSJ9LCJUcmlnZ2VyIjp7Im5hbWUiOiJUcmlnZ2VyIiwiZmlsZSI6InRyaWdnZXIuc3RsIiwiY29sb3IiOiJPcmFuZ2UifSwiU2VhciI6eyJuYW1lIjoiU2VhciIsImZpbGUiOiJzZWFyLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiTXV6emxlIjp7Im5hbWUiOiJNdXp6bGUiLCJmaWxlIjoibXV6emxlLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiVHJlbmNoUCI6eyJuYW1lIjoiVHJlbmNoUCIsImZpbGUiOiJ0cmVuY2hwLnN0bCIsImNvbG9yIjoiT3JhbmdlIn19fSwiTWFnd2VsbCI6eyJuYW1lIjoiRUxJVEUiLCJtb2RlbHMiOnsiRWxpdGUgVXBwZXIiOnsibmFtZSI6IkVsaXRlIFVwcGVyIiwiZmlsZSI6IkVsaXRlVXBwZXIuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJFbGl0ZSBMb3dlciI6eyJuYW1lIjoiRWxpdGUgTG93ZXIiLCJmaWxlIjoiRWxpdGVMb3dlci5zdGwiLCJjb2xvciI6IldoaXRlIn0sIk1hZ1JlbGVhc2UiOnsibmFtZSI6Ik1hZ1JlbGVhc2UiLCJmaWxlIjoibWFncmVsZWFzZTIuc3RsIiwiY29sb3IiOiJPcmFuZ2UifX19LCJHcmlwIjp7Im5hbWUiOiJNb25vbGl0aGljIEdyaXAiLCJtb2RlbHMiOnsiVHJpZ2dlciBHdWFyZCI6eyJuYW1lIjoiVHJpZ2dlciBHdWFyZCIsImZpbGUiOiJ0Z3VhcmQuc3RsIiwiY29sb3IiOiJCbGFjayJ9LCJHcmlwIjp7Im5hbWUiOiJHcmlwIiwiZmlsZSI6ImdyaXAuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJTdG9ja19LaXJpIjp7Im5hbWUiOiJTdG9ja19LaXJpIiwiZmlsZSI6InN0b2NrX2tpcmkuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9fX0sIkhhbmRndWFyZCI6eyJuYW1lIjoiTm9uZSIsIm1vZGVscyI6e319LCJHcmlwIEluc2VydCI6eyJuYW1lIjoiWUVTIiwibW9kZWxzIjp7IkdyaXAgSW5zZXJ0Ijp7Im5hbWUiOiJHcmlwIEluc2VydCIsImZpbGUiOiJncmlwaW5zZXJ0LnN0bCIsImNvbG9yIjoiQmxhY2sifX19LCJTdG9jayBDb3NtZXRpYyI6eyJuYW1lIjoiVGh1bWJob2xlIiwibW9kZWxzIjp7IlRodW1iaG9sZSI6eyJuYW1lIjoiVGh1bWJob2xlIiwiZmlsZSI6InN0b2NrMy5zdGwiLCJjb2xvciI6IldoaXRlIn19fSwiQmFycmVsIFNocm91ZCI6eyJuYW1lIjoiUmFpbGdhc20iLCJtb2RlbHMiOnsiUmFpbGdhc20gRmdyaXAiOnsibmFtZSI6IlJhaWxnYXNtIEZncmlwIiwiZmlsZSI6ImIxX2ZvcmVncmlwLnN0bCIsImNvbG9yIjoiT3JhbmdlIn0sIlJzZWdtZW50MSI6eyJuYW1lIjoiUnNlZ21lbnQxIiwiZmlsZSI6ImIxYS5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlJzZWdtZW50MiI6eyJuYW1lIjoiUnNlZ21lbnQyIiwiZmlsZSI6ImIxYi5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlJzZWdtZW50MyI6eyJuYW1lIjoiUnNlZ21lbnQzIiwiZmlsZSI6ImIxYy5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlJzZWdtZW50NCI6eyJuYW1lIjoiUnNlZ21lbnQ0IiwiZmlsZSI6ImIxZC5zdGwiLCJjb2xvciI6IldoaXRlIn19fSwiRm9yZWdyaXAiOnsibmFtZSI6IlB5cmFuZ2xlIEZvcmVncmlwIiwibW9kZWxzIjp7IlBncmlwIjp7Im5hbWUiOiJQZ3JpcCIsImZpbGUiOiJwZ3JpcDEuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJQZ3JpcCBDb3JlIjp7Im5hbWUiOiJQZ3JpcCBDb3JlIiwiZmlsZSI6InBncmlwMi5zdGwiLCJjb2xvciI6IkJsYWNrIn19fSwiTXV6emxlIERldmljZSI6eyJuYW1lIjoiU0NBUiBTdXBwcmVzc29yIiwibW9kZWxzIjp7IlNDQVIgZnJvbnQiOnsibmFtZSI6IlNDQVIgZnJvbnQiLCJmaWxlIjoic2Nhcl9mcm9udC5zdGwiLCJjb2xvciI6Ik9yYW5nZSJ9LCJTQ0FSIGJhY2siOnsibmFtZSI6IlNDQVIgYmFjayIsImZpbGUiOiJzY2FyX2JhY2suc3RsIiwiY29sb3IiOiJXaGl0ZSJ9fX0sIklyb24gU2lnaHRzIjp7Im5hbWUiOiJZZXMiLCJtb2RlbHMiOnsiSXJvbnMiOnsibmFtZSI6Iklyb25zIiwiZmlsZSI6Imlyb25fc2lnaHRzLnN0bCIsImNvbG9yIjoiT3JhbmdlIn19fSwiUmFpbCBSaXNlciI6eyJuYW1lIjoiUmlzZXIiLCJtb2RlbHMiOnsiUmlzZXIiOnsibmFtZSI6IlJpc2VyIiwiZmlsZSI6IlJpc2VyLnN0bCIsImNvbG9yIjoiQmxhY2sifSwiQ29yZSI6eyJuYW1lIjoiQ29yZSIsImZpbGUiOiJSaXNlckNvcmUuc3RsIiwiY29sb3IiOiJCbGFjayJ9fX0sIlN0b2NrIFR5cGUiOnsibmFtZSI6IkFkanVzdGFibGUiLCJtb2RlbHMiOnsiQVN0b2NrMWEiOnsibmFtZSI6IkFTdG9jazFhIiwiZmlsZSI6IkFTdG9jazFhLnN0bCIsImNvbG9yIjoiQmxhY2sifSwiQVN0b2NrMmEiOnsibmFtZSI6IkFTdG9jazJhIiwiZmlsZSI6IkFTdG9jazJhLnN0bCIsImNvbG9yIjoiT3JhbmdlIn0sIkFTdG9ja1BhZCI6eyJuYW1lIjoiQVN0b2NrUGFkIiwiZmlsZSI6IkFTdG9ja1BhZC5zdGwiLCJjb2xvciI6IkJsYWNrIn19fX19)

![Screenshot 2021-01-05 at 10.41.14](img/Screenshot 2021-01-05 at 10.41.14.png)

Config-Link zu Basisversion: [Link](http://www.captainslug.com/nerf/caliburn/e/#eyJuYW1lIjoiQ2FsaWJ1cm4gUmVkZGl0Iiwic2xvdHMiOnsiQmFzZSI6eyJuYW1lIjoiQ29yZSBDYWxpYnVybiIsIm1vZGVscyI6eyJTcGFjZXIgVHViaW5nIjp7Im5hbWUiOiJTcGFjZXIgVHViaW5nIiwiZmlsZSI6Il9ueWxvbnNwYWNlcnMuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJSYWlsIjp7Im5hbWUiOiJSYWlsIiwiZmlsZSI6InJhaWwuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJNaWxhbiBDb3VwbGVyIjp7Im5hbWUiOiJNaWxhbiBDb3VwbGVyIiwiZmlsZSI6Ik1pbGFuQ291cGxlci5zdGwiLCJjb2xvciI6IldoaXRlIn0sIlBsdW5nZXIiOnsibmFtZSI6IlBsdW5nZXIiLCJmaWxlIjoicGx1bmdlci5zdGwiLCJjb2xvciI6Ik9yYW5nZSJ9LCJUcmlnZ2VyIjp7Im5hbWUiOiJUcmlnZ2VyIiwiZmlsZSI6InRyaWdnZXIuc3RsIiwiY29sb3IiOiJPcmFuZ2UifSwiU2VhciI6eyJuYW1lIjoiU2VhciIsImZpbGUiOiJzZWFyLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiTXV6emxlIjp7Im5hbWUiOiJNdXp6bGUiLCJmaWxlIjoibXV6emxlLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiVHJlbmNoUCI6eyJuYW1lIjoiVHJlbmNoUCIsImZpbGUiOiJ0cmVuY2hwLnN0bCIsImNvbG9yIjoiT3JhbmdlIn19fSwiTWFnd2VsbCI6eyJuYW1lIjoiRUxJVEUiLCJtb2RlbHMiOnsiRWxpdGUgVXBwZXIiOnsibmFtZSI6IkVsaXRlIFVwcGVyIiwiZmlsZSI6IkVsaXRlVXBwZXIuc3RsIiwiY29sb3IiOiJXaGl0ZSJ9LCJFbGl0ZSBMb3dlciI6eyJuYW1lIjoiRWxpdGUgTG93ZXIiLCJmaWxlIjoiRWxpdGVMb3dlci5zdGwiLCJjb2xvciI6IldoaXRlIn0sIk1hZ1JlbGVhc2UiOnsibmFtZSI6Ik1hZ1JlbGVhc2UiLCJmaWxlIjoibWFncmVsZWFzZTIuc3RsIiwiY29sb3IiOiJPcmFuZ2UifX19LCJIYW5kZ3VhcmQiOnsibmFtZSI6Ik5vbmUiLCJtb2RlbHMiOnt9fSwiR3JpcCI6eyJuYW1lIjoiTW9ub2xpdGhpYyBHcmlwIiwibW9kZWxzIjp7IlRyaWdnZXIgR3VhcmQiOnsibmFtZSI6IlRyaWdnZXIgR3VhcmQiLCJmaWxlIjoidGd1YXJkLnN0bCIsImNvbG9yIjoiQmxhY2sifSwiR3JpcCI6eyJuYW1lIjoiR3JpcCIsImZpbGUiOiJncmlwLnN0bCIsImNvbG9yIjoiV2hpdGUifSwiU3RvY2tfS2lyaSI6eyJuYW1lIjoiU3RvY2tfS2lyaSIsImZpbGUiOiJzdG9ja19raXJpLnN0bCIsImNvbG9yIjoiV2hpdGUifX19LCJGb3JlZ3JpcCI6eyJuYW1lIjoiUmFpbCBDb3ZlciIsIm1vZGVscyI6e319LCJTdG9jayBUeXBlIjp7Im5hbWUiOiJRdWljay1DaGFuZ2UiLCJtb2RlbHMiOnsiRnJvbnRCdXR0Ijp7Im5hbWUiOiJGcm9udEJ1dHQiLCJmaWxlIjoiZnJvbnRidXR0LnN0bCIsImNvbG9yIjoiT3JhbmdlIn0sIkJhY2tCdXR0UiI6eyJuYW1lIjoiQmFja0J1dHRSIiwiZmlsZSI6ImJhY2tidXR0LnN0bCIsImNvbG9yIjoiT3JhbmdlIn0sIkJ1dHRQbGF0ZVIiOnsibmFtZSI6IkJ1dHRQbGF0ZVIiLCJmaWxlIjoiYnV0dHBsYXRlLnN0bCIsImNvbG9yIjoiQmxhY2sifX19LCJTdG9jayBDb3NtZXRpYyI6eyJuYW1lIjoiTm9uZSIsIm1vZGVscyI6e319LCJCYXJyZWwgU2hyb3VkIjp7Im5hbWUiOiJCYXNlIEZvcmVncmlwIiwibW9kZWxzIjp7IkJhc2UgRm9yZWdyaXAiOnsibmFtZSI6IkJhc2UgRm9yZWdyaXAiLCJmaWxlIjoiZm9yZWdyaXAuc3RsIiwiY29sb3IiOiJPcmFuZ2UifX19LCJNdXp6bGUgRGV2aWNlIjp7Im5hbWUiOiJNdXp6bGUgQnJha2UiLCJtb2RlbHMiOnsiQ29zbWV0aWMiOnsibmFtZSI6IkNvc21ldGljIiwiZmlsZSI6Im11enpsZWJyYWtlLnN0bCIsImNvbG9yIjoiV2hpdGUifX19LCJJcm9uIFNpZ2h0cyI6eyJuYW1lIjoiTm9uZSIsIm1vZGVscyI6e319LCJSYWlsIFJpc2VyIjp7Im5hbWUiOiJOb25lIiwibW9kZWxzIjp7fX0sIkdyaXAgSW5zZXJ0Ijp7Im5hbWUiOiJOb25lIiwibW9kZWxzIjp7fX19fQ==)

![Screenshot 2021-01-07 at 01.19.48](img/Screenshot 2021-01-07 at 01.19.48.png)

![Screenshot 2021-01-05 at 10.49.47](img/Screenshot 2021-01-05 at 10.49.47.png)



<img src="img/Screenshot 2021-01-05 at 10.41.25.png" alt="Screenshot 2021-01-05 at 10.41.25" style="zoom:50%;" />

## Base Model

| Done?             | Item No. | Quantity | printOrder | Part Name       | Infill % | Config. Name  | Color  |
| ----------------- | -------- | -------- | ---------- | --------------- | -------- | ------------- | ------ |
| done              | 60       | 1        | 8          | Rail            | 20       | Rail          | white  |
| done              | 10       | 1        | 3          | Sear            | 100      | White         | white  |
| done              | 15       | 1        | 4          | Trigger Alt     | 20       | Trigger       | orange |
| done              | 26       | 1        | 7          | MagRelease2     | 20       | MagRelease    | orange |
| **MOD**           | 31       | 1        | 12         | Rail_Foregrip   | 20       | Base_Foregrip | orange |
| done              | 61       | 1        | 6          | EliteLower2     | 20       | EliteLower    | White  |
| done              | 40       | 1        | 2          | Grip5           | 20       | Grip          | White  |
| done              | 41       | 1        | 5          | Tguard7         | 20       | Trigger Guard | Black  |
| done              | 42       | 1        | 13         | Muzzle5         | 20       | Muzzle        | White  |
| done (w)          | 43       | 1        | 17         | PlungerE        | 20       | Plunger       | Orange |
| done              | 44       | 1        | 16         | Ram2            | 20       | Ram           | White  |
| done (w), **MOD** | 45       | 1        | 1          | BackButtR       | 20       | BackButtR     | Orange |
| done              | 62       | 1        | 11         | EliteUpperAlt3  | 20       | EliteUpper    | White  |
| done, **MOD**     | 48       | 1        | 15         | MuzzleBrake2    | 20       | Muzzle Brake  | White  |
| done              | 63       | 1        | 9          | StockKiri2      | 20       | Stock_Kiri    | White  |
| done              | 64       | 1        | 8          | MilanCouplerAlt | 20       | MilanCoupler  | White  |
| done, **MOD**     | 51       | 1        | 10         | FrontButt8      | 20       | FrontButt     | Orange |
| **MOD**           | 53       | 1        | 17         | ButtplateS      | 20       | ButtPlateR    | Black  |
| done              | T        | 1        | 14         | Trench          | 20       | TrenchP       | Orange |



## Mods

### Printed Stock Spacer 

https://www.thingiverse.com/thing:3105159

<img src="img/Screenshot 2021-01-12 at 13.01.41.png" alt="Screenshot 2021-01-12 at 13.01.41" style="zoom:25%;" />

| Done? | Part Name       | Config. Name | Color |
| ----- | --------------- | ------------ | ----- |
| done  | StockSpacerAlt3 |              | white |

### Quick Change: Adjustable Stock

https://www.thingiverse.com/thing:3209370

<img src="img/4777f9c3211c7523a09cb06122346a2d_preview_featured.jpg" alt="4777f9c3211c7523a09cb06122346a2d_preview_featured" style="zoom:50%;" />



| Done? | Part Name  | Config. Name | Color  |
| ----- | ---------- | ------------ | ------ |
| done  | ABackButt  |              | black  |
| done  | AButtPlate |              | black  |
| done  | AStock3    |              | orange |

### Stock Cosmetic: Thumbhole

https://www.thingiverse.com/thing:2849092

<img src="img/6fa0f6dad8f16c86bd272661140d2f45_preview_featured.jpg" alt="6fa0f6dad8f16c86bd272661140d2f45_preview_featured" style="zoom:50%;" />

| Done? | Part Name | Config. Name | Color |
| ----- | --------- | ------------ | ----- |
| done  | Spacer3a  |              | white |

### Barrel Shroud: Railgasm

https://www.thingiverse.com/thing:2722395

<img src="img/7e37bd7b21ac4a3bb6bed23ed0fbeb28_preview_featured.jpg" alt="7e37bd7b21ac4a3bb6bed23ed0fbeb28_preview_featured" style="zoom:50%;" />

| Done? | Quantity | Part Name        | Config. Name | Color  |
| ----- | -------- | ---------------- | ------------ | ------ |
| done  | 1        | BSHROUD_FOREGRIP |              | orange |
| done  | 3        | BSHROUD          |              | white  |
| done  | 1        | BSHROUDFRONT     |              | white  |



### Muzzle Device: SCAR Supressor

https://www.thingiverse.com/thing:2863865

<img src="img/scar2.jpg" alt="scar2" style="zoom:50%;" />

| Done? | Part Name           | Config. Name | Color  |
| ----- | ------------------- | ------------ | ------ |
| done  | Muzzle_SCAR_Cd      |              | orange |
| done  | Muzze_SCAR_HeathNut |              | black  |
| done  | Muzzle_SCAR_Nd      |              | white  |



### Iron Sights: Yes

pip, frontpip included in base package

| Done? | Part Name | Config. Name | Color  |
| ----- | --------- | ------------ | ------ |
| done  | pip       |              | orange |
| done  | frontpip  |              | orange |



### Rail Riser: Riser

https://www.thingiverse.com/thing:3206503

<img src="img/918fab38132446dc4c251059fae7a163_preview_featured.jpg" alt="918fab38132446dc4c251059fae7a163_preview_featured" style="zoom:50%;" />

| Done? | Part Name | Config. Name | Color |
| ----- | --------- | ------------ | ----- |
| done  | Riser     |              | black |
| done  | RiserKey  |              | black |



### Grip Insert: Yes

https://www.thingiverse.com/thing:2849364

<img src="img/c35ef7cadc39551bfdc080a8a397c082_preview_featured.jpg" alt="c35ef7cadc39551bfdc080a8a397c082_preview_featured" style="zoom:50%;" />

| Done? | Part Name   | Config. Name | Color |
| ----- | ----------- | ------------ | ----- |
| done  | Grip5Insert |              | black |



### Foregrip: Pyrangle Foregrip

https://www.thingiverse.com/thing:3180697

<img src="img/c40a7a082c33a980d5dac1031f096c7f_preview_featured.jpg" alt="c40a7a082c33a980d5dac1031f096c7f_preview_featured" style="zoom:50%;" />

| Done? | Part Name      | Config. Name | Color |
| ----- | -------------- | ------------ | ----- |
| done  | PGrip_2tal     |              | black |
| done  | PGrip_2tar     |              | black |
| done  | PGrip_Core_2ta |              | white |

