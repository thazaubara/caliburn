                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2722395
Nerf Caliburn - Railgasm Barrel Shroud by captainslug is licensed under the Creative Commons - Public Domain Dedication license.
http://creativecommons.org/publicdomain/zero/1.0/

# Summary

07/30/18 UPDATE: Fixed screw length issue with foregrip model.

03/17/18 UPDATE: I have added a selective brim that connects the two halves of the barrel shroud segment prints for the first hour of the print cycle. This single-layer section can be trimmed off after the print finishes, but significantly improves the reliability of the individual segment prints.

This is an optional set of four parts (bshroud x3, bshroudfront x1) that replace the stock barrel shroud and upper spacer with stackable printed parts covered in rail segments. You can align them together for gluing using a gaggle of 3/4" length 1/16" diameter spring pins.

You will also either need to cut off the top of your foregrip, or print the included one.

https://www.etsy.com/listing/567730202/